package com.sbk;

import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

@Component
@ImportResource(locations={"classpath:kaptcha.xml"})
public class MyConfig {

}
